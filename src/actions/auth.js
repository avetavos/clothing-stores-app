import { AsyncStorage } from 'react-native';
import api from '../api';
import {
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	LOGOUT,
	USER_LOADED,
	AUTH_ERROR,
	UPDATED_USER,
	UPDATE_FAIL
} from './types';
import setAuthToken from '../utils/setAuthToken';
import { navigate } from '../navigationRef';
import { getAllUser } from './user';

export const loadUser = () => async dispatch => {
	const token = await AsyncStorage.getItem('token');
	await setAuthToken(token);
	if (token) {
		try {
			const response = await api.get('/api/auth/current');
			await dispatch({
				type: USER_LOADED,
				payload: response.data
			});
			if (response.data.role === 'owner') {
				navigate('ownerFlow');
			} else {
				navigate('employeeFlow');
			}
		} catch (e) {
			dispatch({
				type: AUTH_ERROR
			});
		}
	} else {
		return navigate('login');
	}
};

export const login = ({ username, password }) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = { username, password };
	try {
		const response = await api.post('/api/auth/login', body, config);
		await dispatch({
			type: LOGIN_SUCCESS,
			payload: response.data.token
		});
		await dispatch(loadUser());
	} catch (e) {
		dispatch({
			type: LOGIN_FAIL,
			payload: 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง'
		});
	}
};

export const logout = () => async dispatch => {
	navigate('resolveAuth');
	await dispatch({
		type: LOGOUT
	});
};

export const register = ({
	name,
	username,
	password,
	phone,
	facebook,
	role,
	line,
	address
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = {
		name,
		username,
		password,
		phone,
		facebook,
		line,
		role,
		address
	};
	try {
		const response = await api.post('/api/auth/register', body, config);
		await dispatch(getAllUser());
	} catch (e) {
		dispatch({
			type: UPDATE_FAIL
		});
	}
};

export const updateProfile = ({
	_id,
	name,
	username,
	password,
	phone,
	facebook,
	line,
	address
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = {
		name,
		username,
		password,
		phone,
		facebook,
		line,
		address
	};
	try {
		const response = await api.put(`/api/auth/${_id}`, body, config);
		await dispatch({
			type: UPDATED_USER,
			payload: response.data
		});
	} catch (e) {
		dispatch({
			type: UPDATE_FAIL
		});
	}
};
