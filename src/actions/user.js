import api from '../api';
import { GET_ALL_USER, GET_USER, REMOVE_USER, UPDATED_EMPLOYEE } from './types';
import { navigate } from '../navigationRef';

export const getAllUser = () => async dispatch => {
	try {
		const response = await api.get('/api/auth/all');
		dispatch({
			type: GET_ALL_USER,
			payload: response.data
		});
	} catch (e) {
		console.log(e);
	}
};

export const getEmployee = _id => async dispatch => {
	try {
		const response = await api.get(`/api/auth/${_id}`);
		dispatch({
			type: GET_USER,
			payload: response.data
		});
	} catch (e) {
		console.log(e);
	}
};

export const deleteUser = _id => async dispatch => {
	try {
		await api.delete(`/api/auth/${_id}`);
		dispatch({
			type: REMOVE_USER,
			payload: _id
		});
		dispatch(getAllUser());
	} catch (e) {
		dispatch({
			type: UPDATE_FAIL
		});
	}
};

export const updateUser = ({
	_id,
	name,
	username,
	password,
	phone,
	facebook,
	line,
	address
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = {
		name,
		username,
		password,
		phone,
		facebook,
		line,
		address
	};
	try {
		await api.put(`/api/auth/${_id}`, body, config);
		await dispatch(getAllUser());
	} catch (e) {
		console.log(e);
	}
};
