import {
	GET_PRODUCT_BY_BARCODE,
	REMOVE_ITEM_FROM_RECEIPT,
	GET_ALL_RECEIPTS,
	CREATE_RECEIPT
} from './types';
import api from '../api';

export const getAllReceipts = () => async dispatch => {
	try {
		const response = await api.get(`/api/receipts`);
		dispatch({
			type: GET_ALL_RECEIPTS,
			payload: response.data
		});
	} catch (e) {
		console.log(e.messagge);
	}
};

export const createReceipt = body => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		}
	};
	try {
		await api.post('/api/receipts', body, config);
		dispatch(getAllReceipts());
	} catch (e) {
		console.log(e.messagge);
	}
};

export const findProductByBarcode = (barcode, quantity) => async dispatch => {
	try {
		const response = await api.get(`/api/products/barcode/${barcode}/${quantity}`);
		if (response.data) {
			dispatch({
				type: GET_PRODUCT_BY_BARCODE,
				payload: response.data
			});
		}
	} catch (e) {
		console.log(e.messagge);
	}
};

export const removeItemFromReceipt = _id => async dispatch => {
	try {
		dispatch({
			type: REMOVE_ITEM_FROM_RECEIPT,
			payload: _id
		});
	} catch (e) {
		console.log(e.messagge);
	}
};
