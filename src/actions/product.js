import api from '../api';
import { ADD_PRODUCT, GET_ALL_PRODUCT, GET_PRODUCT } from './types';

export const getAllProducts = () => async dispatch => {
	try {
		const response = await api.get('/api/products');
		dispatch({
			type: GET_ALL_PRODUCT,
			payload: response.data
		});
	} catch (e) {
		console.log(e.messagge);
	}
};

export const getProductById = _id => async dispatch => {
	try {
		const response = await api.get(`/api/products/${_id}`);
		dispatch({
			type: GET_PRODUCT,
			payload: response.data
		});
	} catch (e) {
		console.log(e.messagge);
	}
};

export const addProduct = ({ name, barcode, price, cost, quantity }) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = { name, barcode, price, cost, quantity };
	try {
		const response = await api.post('/api/products', body, config);
		dispatch({
			type: ADD_PRODUCT,
			payload: response.data
		});
	} catch (e) {
		console.log(e.messagge);
	}
};

export const updateProduct = ({ _id, name, barcode, price, cost, quantity }) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = {
		name,
		barcode,
		price,
		cost,
		quantity
	};
	try {
		await api.put(`/api/products/${_id}`, body, config);
		dispatch(getAllProducts());
	} catch (e) {
		console.log(e.messagge);
	}
};

export const deleteProduct = _id => async dispatch => {
	try {
		await api.delete(`/api/products/${_id}`);
		dispatch(getAllProducts());
	} catch (e) {
		console.log(e.messagge);
	}
};
