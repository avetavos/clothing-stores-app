import { Platform } from 'react-native';

const createFormData = (photo, body) => {
	const data = new FormData();

	data.append('photo', {
		name: photo.fileName,
		type: photo.type,
		uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', '')
	});

	Object.keys(body).forEach(key => {
		if (typeof body[key] === 'object') {
			data.append(key, JSON.stringify(body[key]));
		} else {
			data.append(key, body[key]);
		}
	});

	return data;
};

export default createFormData;
