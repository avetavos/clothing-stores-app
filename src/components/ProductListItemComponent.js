import React from 'react';
import { View, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { Text } from 'react-native-elements';

const ProductListItem = ({
	product: { _id, name, price, cost, quantity },
	navigation,
	deleteProduct
}) => {
	const onHandleAlert = () => {
		Alert.alert(
			name,
			`ราคาขาย: ${price} บาท \n ราคาต้นทุน: ${cost} บาท \n คงเหลือในคลัง: ${quantity} ชิ้น`,
			[
				{
					text: 'ลบสินค้า',
					onPress: () => onDelAlert()
				},
				{
					text: 'แก้ไขข้อมูล',
					onPress: () => navigation.navigate('ProductDetail', { productId: _id })
				},
				{
					text: 'ปิด',
					onPress: () => {
						return false;
					},
					style: 'cancel'
				}
			],
			{ cancelable: false }
		);
	};

	const onDelAlert = () => {
		Alert.alert(
			'ลบสินค้า',
			`คุณต้องการลบสินค้า "${name}" ใช่หรือไม่`,
			[
				{ text: 'ใช่', onPress: () => deleteProduct(_id) },
				{
					text: 'ไม่',
					onPress: () => {
						return false;
					}
				}
			],
			{ cancelable: false }
		);
	};

	return (
		<TouchableOpacity onPress={() => onHandleAlert()}>
			<View style={styles.item}>
				<Text h4>{name}</Text>
				<View style={styles.itemContent}>
					<View style={styles.detailContainer}>
						<Text style={{ color: '#F56B1B' }}>จำนวนคงเหลือในคลัง :</Text>
						<Text style={{ fontWeight: 'bold', marginLeft: 10, color: '#F56B1B' }}>{quantity}</Text>
						<Text style={{ marginLeft: 10, color: '#F56B1B' }}>ชิ้น</Text>
					</View>
				</View>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	item: {
		backgroundColor: '#fff',
		padding: 10,
		marginTop: 5,
		marginHorizontal: 5
	},
	itemContent: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around'
	},
	detailContainer: {
		marginHorizontal: 2,
		flex: 1,
		justifyContent: 'flex-start',
		flexDirection: 'row',
		padding: 2
	}
});

export default ProductListItem;
