import React, { Fragment } from 'react';
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Text } from 'react-native-elements';

const ReceiptListItem = ({ products, removeProduct }) => {
	const calBackground = index => {
		if (index % 2) {
			return styles.oddContainer;
		} else {
			return styles.evenContainer;
		}
	};

	const onHandleRemove = (name, _id) => {
		Alert.alert(
			'ลบสินค้า',
			`คุณต้องการลบสินค้า "${name}" ออกจากรายการสั่งซื้อใช่หรือไม่`,
			[
				{ text: 'ใช่', onPress: () => removeProduct(_id) },
				{
					text: 'ไม่',
					onPress: () => {
						return false;
					}
				}
			],
			{ cancelable: false }
		);
	};

	return (
		<Fragment>
			{products && products.length > 0 ? (
				products.map((item, index) => (
					<TouchableOpacity
						onPress={() => onHandleRemove(item.name, item._id)}
						key={index}
						style={calBackground(index)}
					>
						<View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
							<Text style={{ fontWeight: 'bold', fontSize: 16 }}>{item.quantity}x</Text>
						</View>
						<View style={{ flex: 1, paddingVertical: 5, paddingHorizontal: 10 }}>
							<Text style={{ fontSize: 16 }}>{item.name}</Text>
						</View>
						<View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
							<Text style={{ fontWeight: 'bold', fontSize: 16 }}>{item.price * item.quantity}</Text>
						</View>
					</TouchableOpacity>
				))
			) : (
				<View style={styles.empty}>
					<Text style={{ color: '#A6A6A6', fontSize: 16 }}>ยังไม่มีรายการสินค้า</Text>
				</View>
			)}
		</Fragment>
	);
};

const styles = StyleSheet.create({
	oddContainer: {
		flexDirection: 'row'
	},
	evenContainer: {
		flexDirection: 'row',
		backgroundColor: '#E9E9E9'
	},
	empty: {
		alignItems: 'center',
		paddingVertical: 30
	}
});

export default ReceiptListItem;
