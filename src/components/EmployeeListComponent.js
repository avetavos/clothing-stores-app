import React, { useState, Fragment } from 'react';
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { deleteUser } from '../actions/user';

const EmployeeListComponent = ({ employee, navigation, deleteUser }) => {
	const [toggleDetail, setToggleDetail] = useState(false);

	const onHandleDelete = () => {
		Alert.alert(
			'ลบพนักงาน',
			`คุณต้องการลบ ${employee.name} ออกจากรายชื่อพนักงานใช่หรือไม่`,
			[
				{
					text: 'ไม่',
					onPress: () => {
						return false;
					},
					style: 'cancel'
				},
				{ text: 'ใช่', onPress: () => deleteUser(employee._id) }
			],
			{ cancelable: false }
		);
	};

	if (employee.role === 'owner') {
		return false;
	} else {
		return (
			<View style={styles.card}>
				<View style={styles.cardBody}>
					<Text h4 style={styles.cardTitle}>
						{employee.name}
					</Text>
					<View style={styles.buttonContainer}>
						<TouchableOpacity onPress={() => setToggleDetail(!toggleDetail)}>
							<View style={styles.moreButton}>
								{toggleDetail ? (
									<Icon name='ios-remove' type='ionicon' style={{ fontWeight: 'bold' }} />
								) : (
									<Icon name='ios-add' type='ionicon' style={{ fontWeight: 'bold' }} />
								)}
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => navigation.navigate('EditEmployee', { userId: employee._id })}
						>
							<View style={styles.editButton}>
								<Icon name='pencil' type='font-awesome' size={18} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => onHandleDelete()}>
							<View style={styles.delButton}>
								<Icon name='trash' type='font-awesome' size={18} color='#fff' />
							</View>
						</TouchableOpacity>
					</View>
				</View>
				{toggleDetail ? (
					<Fragment>
						{!employee.facebook && !employee.line && !employee.phone ? (
							<View style={{ alignItems: 'center', paddingVertical: 10 }}>
								<Text>ยังไม่มีข้อมูล</Text>
							</View>
						) : null}
						{employee.facebook ? (
							<View style={styles.detailContainer}>
								<View style={styles.facebook}>
									<Icon name='facebook-square' type='font-awesome' color='#fff' size={20} />
									<Text style={{ color: '#fff' }}>{employee.facebook}</Text>
								</View>
							</View>
						) : null}
						{employee.line ? (
							<View style={styles.detailContainer}>
								<View style={styles.line}>
									<Text style={{ color: '#fff' }}>LineID</Text>
									<Text style={{ color: '#fff', fontWeight: 'bold' }}>{employee.line}</Text>
								</View>
							</View>
						) : null}
						{employee.phone ? (
							<View style={styles.detailContainer}>
								<View style={styles.phone}>
									<Icon name='phone' type='font-awesome' color='#fff' size={20} />
									<Text style={{ color: '#fff' }}>{employee.phone}</Text>
								</View>
							</View>
						) : null}
					</Fragment>
				) : null}
			</View>
		);
	}
};

const styles = StyleSheet.create({
	card: {
		marginHorizontal: 10,
		marginTop: 10,
		backgroundColor: '#fff',
		paddingVertical: 5,
		paddingHorizontal: 10,
		borderRadius: 5,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		flex: 1,
		elevation: 5
	},
	cardTitle: {
		fontWeight: 'bold',
		color: '#242424'
	},
	cardBody: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 5
	},
	buttonContainer: { flex: 1, flexDirection: 'row-reverse' },
	moreButton: {
		backgroundColor: '#F29C94',
		width: 30,
		height: 30,
		borderRadius: 50,
		display: 'flex',
		justifyContent: 'center',
		alignContent: 'center'
	},
	editButton: {
		backgroundColor: '#FFCD40',
		width: 30,
		height: 30,
		borderRadius: 50,
		display: 'flex',
		justifyContent: 'center',
		alignContent: 'center',
		marginRight: 5
	},
	delButton: {
		backgroundColor: '#D72C39',
		width: 30,
		height: 30,
		borderRadius: 50,
		display: 'flex',
		justifyContent: 'center',
		alignContent: 'center',
		marginRight: 5
	},
	facebook: {
		backgroundColor: '#1DBAEA',
		paddingHorizontal: 10,
		paddingVertical: 3,
		borderRadius: 5,
		flex: 1,
		marginBottom: 3,
		justifyContent: 'space-between',
		flexDirection: 'row'
	},
	line: {
		backgroundColor: '#1D852E',
		paddingHorizontal: 10,
		paddingVertical: 3,
		borderRadius: 5,
		flex: 1,
		marginBottom: 3,
		justifyContent: 'space-between',
		flexDirection: 'row'
	},
	phone: {
		backgroundColor: '#FFB657',
		paddingHorizontal: 10,
		paddingVertical: 3,
		borderRadius: 5,
		flex: 1,
		marginBottom: 3,
		justifyContent: 'space-between',
		flexDirection: 'row'
	},
	detailContainer: { flex: 1, flexDirection: 'column' }
});

export default connect(
	null,
	{ deleteUser }
)(EmployeeListComponent);
