import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/th';

moment().locale('th');

const ReceiptList = ({ receipt, role, navigation }) => {
	const onHandleAlert = () => {
		navigation.navigate('ReceiptDetail', { receipt });
	};

	return (
		<TouchableOpacity onPress={role === 'owner' ? () => onHandleAlert() : null}>
			<View style={styles.item}>
				<View
					style={{
						...styles.detailContainer,
						flexDirection: 'column',
						borderBottomColor: '#191919',
						borderBottomWidth: 1,
						paddingBottom: 5,
						marginBottom: 5
					}}
				>
					<Text style={{ fontSize: 18 }}>เลขที่ใบเสร็จ</Text>
					<Text style={{ fontSize: 18, fontWeight: 'bold', color: '#D98484' }}>{receipt._id}</Text>
				</View>
				<View style={styles.itemContent}>
					<View style={styles.detailContainer}>
						<Text>ออกโดย :</Text>
						<Text style={{ fontWeight: 'bold', marginLeft: 10 }}>{receipt.user.name}</Text>
					</View>
					<View style={styles.detailContainer}>
						<Text>ออกเมื่อวันที่ :</Text>
						<Text style={{ fontWeight: 'bold', marginLeft: 10 }}>
							{moment(receipt.createdAt).format('DD/MM/YYYY HH:mm:ss')}
						</Text>
					</View>
					<View style={styles.detailContainer}>
						<Text style={{ color: '#087F31' }}>มูลค่าใบเสร็จ :</Text>
						<Text style={{ fontWeight: 'bold', marginLeft: 10, color: '#087F31' }}>
							{receipt.list.reduce((total, item) => {
								return total + item.price * item.quantity;
							}, 0)}
						</Text>
						<Text style={{ marginLeft: 10, color: '#087F31' }}>บาท</Text>
					</View>
				</View>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	item: {
		backgroundColor: '#fff',
		padding: 10,
		marginTop: 5,
		marginHorizontal: 5
	},
	itemContent: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around'
	},
	detailContainer: {
		marginHorizontal: 2,
		flex: 1,
		justifyContent: 'flex-start',
		flexDirection: 'row',
		padding: 2
	}
});

export default ReceiptList;
