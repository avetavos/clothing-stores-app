import React from 'react';
import { View } from 'react-native';
import { Text, Input } from 'react-native-elements';

const ProductSearch = ({ setKeyword }) => {
	return (
		<View style={{ paddingVertical: 5, backgroundColor: '#F6E3E3' }}>
			<Input
				placeholder={'ชื่อสินค้าที่ต้องการค้นหา'}
				inputStyle={{ borderRadius: 50, backgroundColor: '#fff', paddingHorizontal: 10 }}
				inputContainerStyle={{ borderBottomWidth: 0 }}
				onChangeText={text => setKeyword(text)}
			/>
		</View>
	);
};

export default ProductSearch;
