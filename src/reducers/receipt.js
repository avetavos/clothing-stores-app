import {
	GET_PRODUCT_BY_BARCODE,
	REMOVE_ITEM_FROM_RECEIPT,
	GET_ALL_RECEIPTS,
	CREATE_RECEIPT
} from '../actions/types';

const initialState = {
	productsList: [],
	receipts: [],
	receipt: []
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case GET_ALL_RECEIPTS:
			return {
				...state,
				receipts: payload
			};
		case CREATE_RECEIPT:
			return {
				...state,
				receipts: [state.receipts, payload]
			};
		case GET_PRODUCT_BY_BARCODE:
			return {
				...state,
				productsList: [...state.productsList, payload]
			};
		case REMOVE_ITEM_FROM_RECEIPT:
			return {
				...state,
				productsList: state.productsList.filter(item => item._id !== payload)
			};
		default:
			return state;
	}
};
