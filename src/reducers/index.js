import { combineReducers } from 'redux';
import auth from './auth';
import error from './error';
import user from './user';
import product from './product';
import receipt from './receipt';

export default combineReducers({
	auth,
	user,
	product,
	receipt,
	error
});
