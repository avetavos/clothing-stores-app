import { GET_ALL_USER, GET_USER } from '../actions/types';

const initialState = {
	list: null,
	editing: null
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case GET_ALL_USER:
			return {
				...state,
				list: [...payload]
			};
		case GET_USER:
			return {
				...state,
				editing: payload
			};
		default:
			return state;
	}
};
