import { LOGIN_FAIL } from '../actions/types';

const initialState = {
	message: '',
	status: ''
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case LOGIN_FAIL:
			return {
				message: payload,
				status: 'danger'
			};
		default:
			return state;
	}
};
