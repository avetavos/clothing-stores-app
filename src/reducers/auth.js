import { AsyncStorage } from 'react-native';
import { LOGIN_SUCCESS, USER_LOADED, LOGOUT, UPDATED_USER } from '../actions/types';

const initialState = {
	token: AsyncStorage.getItem('token'),
	isAuthenticated: null,
	user: null
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case LOGIN_SUCCESS:
			AsyncStorage.setItem('token', payload);
			return {
				...state,
				token: payload
			};
		case USER_LOADED:
			return {
				...state,
				user: payload,
				isAuthenticated: true
			};
		case UPDATED_USER:
			return {
				...state,
				user: payload
			};
		case LOGOUT:
			AsyncStorage.removeItem('token');
			return {
				...state,
				isAuthenticated: null,
				user: null
			};
		default:
			return state;
	}
};
