import { ADD_PRODUCT, GET_ALL_PRODUCT, GET_PRODUCT, UPDATE_PRODUCT } from '../actions/types';

const initialState = {
	products: null,
	product: null
};

export default (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case GET_ALL_PRODUCT:
			return {
				...state,
				products: payload
			};
		case GET_PRODUCT:
			return {
				...state,
				product: payload
			};
		case ADD_PRODUCT:
			return {
				...state,
				products: [...state.products, payload]
			};
		default:
			return state;
	}
};
