import axios from 'axios';

export const baseURL = 'https://afternoon-thicket-66569.herokuapp.com';

export default axios.create({
	baseURL
});
