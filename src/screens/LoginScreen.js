import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { Text, Input } from 'react-native-elements';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import { login } from '../actions/auth';
import BackgroundImage from '../../assets/splash.png'

const LoginScreen = ({ login, error }) => {
	const [formData, setFormData] = useState({
		username: '',
		password: ''
	});
	return (
		<View style={styles.container}>
			<ImageBackground source={BackgroundImage} style={{width: '100%', height: '100%'}}><SafeAreaView forceInset={{ top: 'always' }}>
			<View style={{ marginBottom: 12, paddingHorizontal: 24, marginTop: 300 }}>
				
				<View style={styles.spacer}>
					<Input
						placeholder='ชื่อผู้ใช้'
						value={formData.username}
						onChangeText={username => setFormData({ ...formData, username })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						placeholder='รหัสผ่าน'
						value={formData.password}
						onChangeText={password => setFormData({ ...formData, password })}
						secureTextEntry={true}
					/>
				</View>
				{error.message ? (
					<View style={{ ...styles.spacer, marginHorizontal: 10 }}>
						<Text style={{ color: 'red' }}>{error.message}</Text>
					</View>
				) : null}
				<TouchableOpacity
					onPress={() => login(formData)}
					style={{ ...styles.button, backgroundColor: '#fff' }}
				>
					<Text style={{ ...styles.buttonTitle, color: '#D98484' }}>เข้าสู่ระบบ</Text>
				</TouchableOpacity></View></SafeAreaView>
  </ImageBackground>
				
			
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#E8E5E0',
		flex: 1,
	},
	spacer: {
		marginVertical: 6
	},
	button: {
		paddingVertical: 20,
		marginHorizontal: 40,
		marginTop: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	}
});

const mapStateToProps = state => ({
	error: state.error
});

export default connect(
	mapStateToProps,
	{ login }
)(LoginScreen);
