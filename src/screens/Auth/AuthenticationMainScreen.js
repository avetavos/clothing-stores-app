import React from 'react';
import { StyleSheet, Alert, TouchableOpacity, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';

const AuthenticationScreen = ({ navigation, logout, user }) => {
	const onHaddleSignOut = () => {
		Alert.alert(
			'ออกจากระบบ',
			'คุณต้องการออกจากระบบใช่หรือไม่',
			[
				{
					text: 'ไม่',
					onPress: () => {
						return false;
					},
					style: 'cancel'
				},
				{ text: 'ใช่', onPress: () => logout() }
			],
			{ cancelable: false }
		);
	};
	return (
		<SafeAreaView style={styles.container}>
			{user.role === 'owner' ? (
				<TouchableOpacity
					onPress={() => navigation.navigate('UpdateProfile')}
					style={{ ...styles.card, backgroundColor: 'rgba(255, 193, 7, 0.8)' }}
				>
					<Text style={styles.cardTitle}>แก้ไขข้อมูลส่วนตัว</Text>
				</TouchableOpacity>
			) : null}
			<TouchableOpacity
				onPress={() => onHaddleSignOut()}
				style={{ ...styles.card, backgroundColor: 'rgb(220, 53, 69)' }}
			>
				<Text style={{ ...styles.cardTitle, color: '#fff' }}>ออกจากระบบ</Text>
			</TouchableOpacity>
		</SafeAreaView>
	);
};

AuthenticationScreen.navigationOptions = () => {
	return {
		header: null
	};
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#E8E5E0',
		flex: 1,
		justifyContent: 'flex-start',
		flexDirection: 'column',
		paddingTop: Platform.OS === 'android' ? 25 : 0
	},
	card: {
		marginTop: 40,
		paddingVertical: 20,
		marginHorizontal: 40,
		borderRadius: 50
	},
	cardTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	}
});

const mapStateToProps = state => ({
	user: state.auth.user
});

export default connect(
	mapStateToProps,
	{ logout }
)(AuthenticationScreen);
