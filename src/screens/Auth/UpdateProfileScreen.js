import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView, Alert, TouchableOpacity } from 'react-native';
import { Input, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { updateProfile } from '../../actions/auth';

const ChangePassowordScreen = ({ updateProfile, user, navigation }) => {
	const [formData, setFormData] = useState({
		_id: '',
		name: '',
		username: '',
		password: '',
		confirmPassword: '',
		phone: '',
		facebook: '',
		line: '',
		address: ''
	});
	useEffect(() => {
		setFormData({
			_id: user._id,
			name: user.name,
			username: user.username,
			phone: user.phone,
			facebook: user.facebook,
			line: user.line,
			address: user.address
		});
	}, []);
	const onSubmit = async () => {
		if (formData.password || formData.confirmPassword) {
			if (formData.password === formData.confirmPassword) {
				await updateProfile(formData);
				navigation.navigate('MainAuth');
			} else {
				return Alert.alert(
					'ผิดพลาด',
					'รหัสผ่านไม่ตรงกันกรุณาระบุรหัสผ่านที่ต้องการเปลี่ยนใหม่าอีกครั้ง'
				);
			}
		}
		await updateProfile(formData);
		navigation.navigate('MainAuth');
	};
	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={styles.spacer}>
					<Input
						labelStyle={{ color: '#D98484', marginBottom: -5 }}
						label='ชื่อ นามสกุล'
						value={formData.name}
						onChangeText={name => setFormData({ ...formData, name })}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ชื่อผู้ใช้'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.username}
						disabled={true}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='รหัสผ่าน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.password}
						onChangeText={password => setFormData({ ...formData, password })}
						secureTextEntry={true}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ยืนยันรหัสผ่าน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.confirmPassword}
						onChangeText={confirmPassword => setFormData({ ...formData, confirmPassword })}
						secureTextEntry={true}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='เบอร์โทรศัพท์ที่สามารถติดต่อได้'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.phone}
						onChangeText={phone => setFormData({ ...formData, phone })}
						autoCapitalize='none'
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='Facebook'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.facebook}
						onChangeText={facebook => setFormData({ ...formData, facebook })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='Line ID'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.line}
						onChangeText={line => setFormData({ ...formData, line })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ที่อยู่ปัจจุบัน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.address}
						onChangeText={address => setFormData({ ...formData, address })}
						autoCapitalize='none'
						multiline
						numberOfLines={4}
					/>
				</View>
				<TouchableOpacity
					onPress={() => onSubmit()}
					style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
				>
					<Text style={styles.buttonTitle}>บันทึก</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	);
};

ChangePassowordScreen.navigationOptions = () => {
	return {
		title: 'แก้ไขข้อมูลส่วนตัว',
		headerStyle: {
			backgroundColor: 'rgba(255, 193, 7, 0.8)'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10,
		backgroundColor: '#E8E5E0'
	},
	spacer: {
		marginVertical: 12,
		paddingHorizontal: 12
	},
	button: {
		paddingVertical: 20,
		margin: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	}
});

const mapStateToProps = state => ({
	user: state.auth.user
});

export default connect(
	mapStateToProps,
	{ updateProfile }
)(ChangePassowordScreen);
