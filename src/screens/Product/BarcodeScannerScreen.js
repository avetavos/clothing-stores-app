import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';

const BarcodeScannerScreen = ({ navigation }) => {
	const [hasCameraPermission, setCameraPermission] = useState(null);

	useEffect(() => {
		getPermissionsAsync();
	}, []);

	const getPermissionsAsync = async () => {
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		setCameraPermission({ status: 'granted' });
	};

	const handleBarCodeScanned = ({ type, data }) => {
		navigation.navigate(navigation.getParam('screen'), { barcodeValue: data });
	};

	if (hasCameraPermission === null) {
		return <Text>Requesting for camera permission</Text>;
	}
	if (hasCameraPermission === false) {
		return <Text>No access to camera</Text>;
	}

	return (
		<View
			style={{
				flex: 1,
				justifyContent: 'flex-start'
			}}
		>
			<BarCodeScanner onBarCodeScanned={handleBarCodeScanned} style={{ flex: 10 }} />
		</View>
	);
};

BarcodeScannerScreen.navigationOptions = () => {
	return {
		title: `สแกนสินค้า`
	};
};

export default BarcodeScannerScreen;
