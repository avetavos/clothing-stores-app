import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { Text, Button, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { addProduct } from '../../actions/product';

const ProductCreateScreen = ({ navigation, addProduct }) => {
	const [product, setProduct] = useState({
		name: '',
		barcode: '',
		price: '',
		cost: '',
		quantity: ''
	});

	useEffect(() => {
		setProduct({ ...product, barcode: navigation.getParam('barcodeValue') });
	}, [navigation.getParam('barcodeValue')]);

	const onHandleSubmit = async () => {
		await addProduct(product);
		navigation.navigate('ProductList');
	};

	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={styles.spacer}>
					<Input
						label='ชื่อสินค้า'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={product.name}
						onChangeText={name => setProduct({ ...product, name })}
					/>
				</View>
				<View style={{ ...styles.spacer, flex: 1, flexDirection: 'row' }}>
					<View style={{ flex: 10 }}>
						<Input
							label='Barcode'
							labelStyle={{ color: '#D98484', marginBottom: 0 }}
							onChangeText={barcode => setProduct({ ...product, barcode })}
							value={product.barcode}
							autoCapitalize='none'
						/>
					</View>
					<View style={{ justifyContent: 'center' }}>
						<Button
							type='outline'
							icon={{ name: 'barcode', type: 'font-awesome' }}
							onPress={() => navigation.navigate('BarcodeProduct', { screen: 'ProductCreate' })}
						/>
					</View>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ราคาขาย'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={product.price}
						onChangeText={price => setProduct({ ...product, price })}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ราคาต้นทุน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={product.cost}
						onChangeText={cost => setProduct({ ...product, cost })}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='จำนวน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={product.quantity}
						onChangeText={quantity => setProduct({ ...product, quantity })}
						keyboardType={'numeric'}
					/>
				</View>
				<TouchableOpacity
					onPress={() => onHandleSubmit()}
					style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
				>
					<Text style={styles.buttonTitle}>บันทึก</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	);
};

ProductCreateScreen.navigationOptions = () => {
	return {
		title: 'สร้างรายการสินค้าใหม่',
		headerStyle: {
			backgroundColor: '#7DDFB5'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10,
		backgroundColor: '#E8E5E0'
	},
	spacer: {
		marginVertical: 12,
		paddingHorizontal: 12
	},
	button: {
		paddingVertical: 20,
		margin: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	},
	picker: {
		height: 50,
		alignSelf: 'stretch',
		marginHorizontal: -9,
		marginBottom: -5
	}
});

export default connect(
	null,
	{ addProduct }
)(ProductCreateScreen);
