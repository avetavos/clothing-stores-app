import React, { useEffect, Fragment, useState } from 'react';
import { View, FlatList, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Text, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { getAllProducts, deleteProduct } from '../../actions/product';
import ProductList from '../../components/ProductListItemComponent';
import ProductSearch from '../../components/ProductSearchComponent';

const ProductListScreen = ({ navigation, products, getAllProducts, deleteProduct }) => {
	const [keyword, setKeyword] = useState('');

	useEffect(() => {
		getAllProducts();
	}, []);

	let productList = null;

	if (keyword) {
		var regex = new RegExp(keyword);
		productList =
			products && products.filter(item => item.name.match(regex)).length > 0 ? (
				<FlatList
					data={products.filter(item => item.name.match(regex))}
					renderItem={({ item }) => (
						<ProductList product={item} navigation={navigation} deleteProduct={deleteProduct} />
					)}
					keyExtractor={item => item._id}
				/>
			) : (
				<View style={styles.empty}>
					<Text h3>ไม่มีสินค้า</Text>
				</View>
			);
	} else {
		productList =
			products && products.length > 0 ? (
				<FlatList
					data={products}
					renderItem={({ item }) => (
						<ProductList product={item} navigation={navigation} deleteProduct={deleteProduct} />
					)}
					keyExtractor={item => item._id}
				/>
			) : (
				<View style={styles.empty}>
					<Text h3>ไม่มีสินค้า</Text>
				</View>
			);
	}

	return (
		<SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
			<ProductSearch setKeyword={setKeyword} />
			{productList}
			<TouchableOpacity
				style={styles.addButton}
				onPress={() => navigation.navigate('ProductCreate')}
			>
				<Icon name='plus' type='font-awesome' color='#000' />
			</TouchableOpacity>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E8E5E0',
		paddingTop: Platform.OS === 'android' ? 25 : 0
	},
	empty: {
		marginTop: 50,
		alignItems: 'center',
		flex: 1
	},
	addButton: {
		position: 'absolute',
		bottom: 0,
		right: 0,
		zIndex: 10,
		marginRight: 10,
		marginBottom: 10,
		backgroundColor: '#D98484',
		height: 50,
		width: 50,
		alignContent: 'center',
		justifyContent: 'center',
		borderRadius: 50
	}
});

ProductListScreen.navigationOptions = () => {
	return {
		header: null
	};
};

const mapStateToProps = state => ({
	products: state.product.products
});

export default connect(
	mapStateToProps,
	{ getAllProducts, deleteProduct }
)(ProductListScreen);
