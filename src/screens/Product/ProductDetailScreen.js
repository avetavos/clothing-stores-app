import React, { useEffect, useState } from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { getProductById, updateProduct } from '../../actions/product';

const ProductDetailScreen = ({ navigation, product, getProductById, updateProduct }) => {
	const [productDetail, setProductDetail] = useState({
		_id: '',
		name: '',
		barcode: '',
		price: '',
		cost: '',
		quantity: ''
	});

	useEffect(() => {
		getProductById(navigation.getParam('productId'));
	}, []);

	useEffect(() => {
		if (product) {
			setProductDetail({
				_id: product._id,
				name: product.name,
				barcode: product.barcode,
				price: `${product.price}`,
				cost: `${product.cost}`,
				quantity: `${product.quantity}`
			});
		}
	}, [product, getProductById]);

	useEffect(() => {
		setProductDetail({ ...productDetail, barcode: navigation.getParam('barcodeValue') });
	}, [navigation.getParam('barcodeValue')]);

	const onHandleSubmit = async () => {
		await updateProduct(productDetail);
		navigation.navigate('ProductList');
	};
	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={styles.spacer}>
					<Input
						label='ชื่อสินค้า'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={productDetail.name}
						onChangeText={name => setProductDetail({ ...productDetail, name })}
					/>
				</View>
				<View style={{ ...styles.spacer, flex: 1, flexDirection: 'row' }}>
					<View style={{ flex: 10 }}>
						<Input
							label='Barcode'
							labelStyle={{ color: '#D98484', marginBottom: 0 }}
							onChangeText={barcode => setProductDetail({ ...productDetail, barcode })}
							value={productDetail.barcode}
							autoCapitalize='none'
						/>
					</View>
					<View style={{ justifyContent: 'center' }}>
						<Button
							type='outline'
							icon={{ name: 'barcode', type: 'font-awesome' }}
							onPress={() => navigation.navigate('BarcodeProduct', { screen: 'ProductDetail' })}
						/>
					</View>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ราคาขาย'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={productDetail.price}
						onChangeText={price => setProductDetail({ ...productDetail, price })}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ราคาต้นทุน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={productDetail.cost}
						onChangeText={cost => setProductDetail({ ...productDetail, cost })}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='จำนวน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={productDetail.quantity}
						onChangeText={quantity => setProductDetail({ ...productDetail, quantity })}
						keyboardType={'numeric'}
					/>
				</View>
				<TouchableOpacity
					onPress={() => onHandleSubmit()}
					style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
				>
					<Text style={styles.buttonTitle}>บันทึก</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	);
};

ProductDetailScreen.navigationOptions = () => {
	return {
		title: 'แก้ไขข้อมูลสินค้า',
		headerStyle: {
			backgroundColor: 'rgba(255, 193, 7, 0.8)'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10,
		backgroundColor: '#E8E5E0'
	},
	spacer: {
		marginVertical: 12,
		paddingHorizontal: 12
	},
	button: {
		paddingVertical: 20,
		margin: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	},
	picker: {
		height: 50,
		alignSelf: 'stretch',
		marginHorizontal: -9,
		marginBottom: -5
	}
});

const mapStateToProps = state => ({
	product: state.product.product
});

export default connect(
	mapStateToProps,
	{ getProductById, updateProduct }
)(ProductDetailScreen);
