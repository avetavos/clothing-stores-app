import React, { useEffect } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import { Text } from 'react-native-elements';
import EmployeeListComponent from '../../components/EmployeeListComponent';
import { connect } from 'react-redux';
import { getAllUser } from '../../actions/user';

const EmployeeListScreen = ({ user, navigation, getAllUser }) => {
	useEffect(() => {
		getAllUser();
	}, []);

	return (
		<View style={{ backgroundColor: '#E8E5E0', flex: 1 }}>
			<SafeAreaView style={styles.container}>
				<ScrollView forceInset={{ top: 'always' }}>
					{user &&
						user.map((item, index) => (
							<EmployeeListComponent key={index} navigation={navigation} employee={item} />
						))}
					<TouchableOpacity
						onPress={() => navigation.navigate('CreateEmployee')}
						style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
					>
						<Text style={{ ...styles.buttonTitle }}>สร้างพนักงานใหม่</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => navigation.navigate('CreateCoOwner')}
						style={{ ...styles.button, backgroundColor: '#F29C94' }}
					>
						<Text style={{ ...styles.buttonTitle }}>สร้างเจ้าของกิจการร่วม</Text>
					</TouchableOpacity>
				</ScrollView>
			</SafeAreaView>
		</View>
	);
};

EmployeeListScreen.navigationOptions = () => {
	return {
		header: null
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E8E5E0',
		paddingTop: Platform.OS === 'android' ? 25 : 0
	},
	button: {
		paddingVertical: 20,
		marginHorizontal: 40,
		marginTop: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	}
});

const mapStateToProps = state => ({
	user: state.user.list
});

export default connect(
	mapStateToProps,
	{ getAllUser }
)(EmployeeListScreen);
