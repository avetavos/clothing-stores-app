import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, Alert, TouchableOpacity, Picker } from 'react-native';
import { Input, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { register } from '../../actions/auth';

const EmployeeCreateScreen = ({ register, navigation }) => {
	const [formData, setFormData] = useState({
		name: '',
		username: '',
		password: '',
		confirmPassword: '',
		role: 'employee',
		phone: '',
		facebook: '',
		line: '',
		address: ''
	});

	const onSubmit = async () => {
		if (!formData.name) {
			return Alert.alert('ผิดพลาด', 'กรุณากำหนดชื่อ');
		}
		if (!formData.username) {
			return Alert.alert('ผิดพลาด', 'กรุณากำหนดชื่อผู้ใช้');
		}
		if (!formData.password) {
			return Alert.alert('ผิดพลาด', 'กรุณากำหนดชื่อรหัสผ่าน');
		}
		if (formData.password || formData.confirmPassword) {
			if (formData.password === formData.confirmPassword) {
				await register(formData);
				navigation.navigate('mainList');
			} else {
				return Alert.alert(
					'ผิดพลาด',
					'รหัสผ่านไม่ตรงกันกรุณาระบุรหัสผ่านที่ต้องการเปลี่ยนใหม่าอีกครั้ง'
				);
			}
		}
	};
	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={styles.spacer}>
					<Input
						label='ชื่อ นามสกุล'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.name}
						onChangeText={name => setFormData({ ...formData, name })}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ชื่อผู้ใช้'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						onChangeText={username => setFormData({ ...formData, username })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='รหัสผ่าน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.password}
						onChangeText={password => setFormData({ ...formData, password })}
						secureTextEntry={true}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ยืนยันรหัสผ่าน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.confirmPassword}
						onChangeText={confirmPassword => setFormData({ ...formData, confirmPassword })}
						secureTextEntry={true}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='เบอร์โทรศัพท์ที่สามารถติดต่อได้'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.phone}
						onChangeText={phone => setFormData({ ...formData, phone })}
						autoCapitalize={'none'}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='Facebook'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.facebook}
						onChangeText={facebook => setFormData({ ...formData, facebook })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='Line ID'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.line}
						onChangeText={line => setFormData({ ...formData, line })}
						autoCapitalize='none'
					/>
				</View>
				<View style={styles.spacer}>
					<Input
						label='ที่อยู่ปัจจุบัน'
						labelStyle={{ color: '#D98484', marginBottom: 0 }}
						value={formData.address}
						onChangeText={address => setFormData({ ...formData, address })}
						autoCapitalize='none'
						multiline
						numberOfLines={4}
					/>
				</View>
				<TouchableOpacity
					onPress={() => onSubmit()}
					style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
				>
					<Text style={styles.buttonTitle}>บันทึก</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	);
};

EmployeeCreateScreen.navigationOptions = () => {
	return {
		title: 'สร้างพนักงานใหม่',
		headerStyle: {
			backgroundColor: '#7DDFB5'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 10,
		backgroundColor: '#E8E5E0'
	},
	spacer: {
		marginVertical: 12,
		paddingHorizontal: 12
	},
	button: {
		paddingVertical: 20,
		margin: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	},
	picker: {
		height: 50,
		alignSelf: 'stretch',
		marginHorizontal: -9,
		marginBottom: -5
	}
});

export default connect(
	null,
	{ register }
)(EmployeeCreateScreen);
