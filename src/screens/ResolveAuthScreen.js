import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { loadUser } from '../actions/auth';

const ResolveAuthScreen = ({ loadUser }) => {
	useEffect(() => {
		loadUser();
	}, []);
	return null;
};

export default connect(
	null,
	{ loadUser }
)(ResolveAuthScreen);
