import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native';
import { Text, Input } from 'react-native-elements';
import ReceiptListItem from '../../components/ReceiptListItemComponent';
import { connect } from 'react-redux';
import { findProductByBarcode, removeItemFromReceipt, createReceipt } from '../../actions/receipt';
import { updateProduct } from '../../actions/product';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import createFormData from '../../utils/createFormData';

const ReceiptCreateScreen = ({
	navigation,
	productsList,
	findProductByBarcode,
	removeItemFromReceipt,
	createReceipt,
	updateProduct
}) => {
	const [receiptDetail, setReceiptDetail] = useState('');

	const [receiptImages, setReceiptImages] = useState(null);

	useEffect(() => {
		getPermissionAsync();
	}, []);

	useEffect(() => {
		if (navigation.getParam('barcodeValue')) {
			const barcode = navigation.getParam('barcodeValue');
			const quantity = navigation.getParam('quantityValue');
			findProductByBarcode(barcode, quantity);
		}
	}, [navigation.getParam('barcodeValue'), navigation.getParam('quantityValue')]);

	const getPermissionAsync = async () => {
		if (Constants.platform.ios) {
			const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
			if (status !== 'granted') {
				alert('Sorry, we need camera roll permissions to make this work!');
			}
		}
	};

	const onHandlePickImage = async () => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.Images,
			quality: 0.2
		});
		if (!result.cancelled) {
			let uri = await result.uri;
			let fileName = await uri.split('/').pop();
			let match = await /\.(\w+)$/.exec(fileName);
			let type = (await match) ? `image/${match[1]}` : `image`;
			setReceiptImages({
				cancelled: result.cancelled,
				type,
				fileName,
				uri,
				match
			});
		}
	};

	const onHandleSubmit = async (slip, products, detail) => {
		const data = await createFormData(slip, { list: products, description: detail });
		await createReceipt(data);
		products.forEach(async item => {
			await updateProduct({
				_id: item._id,
				name: item.name,
				barcode: item.barcode,
				price: item.price,
				cost: item.cost,
				quantity: item.store - item.quantity
			});
		});
		navigation.navigate('ReceiptList');
	};

	return (
		<View style={styles.container}>
			<ScrollView style={{ padding: 5 }}>
				<View style={styles.inputGroup}>
					<View style={styles.headerContainer}>
						<Text style={styles.headerText}>รายละเอียดสินค้า</Text>
						<TouchableOpacity
							style={styles.headerButton}
							onPress={() => navigation.navigate('BarcodeReceipt', { screen: 'ReceiptCreate' })}
						>
							<Text style={styles.headerButtonText}>เพิ่มสินค้า</Text>
						</TouchableOpacity>
					</View>
					<ReceiptListItem products={productsList} removeProduct={removeItemFromReceipt} />
				</View>
				<View style={styles.inputGroup}>
					<View style={styles.headerContainer}>
						<Text style={styles.headerText}>รายละเอียดใบเสร็จ</Text>
					</View>
					<Input
						multiline
						numberOfLines={4}
						inputContainerStyle={{ borderBottomWidth: 0, marginTop: 5, marginHorizontal: -10 }}
						inputStyle={styles.detailInput}
						value={receiptDetail}
						onChangeText={text => setReceiptDetail(text)}
					/>
				</View>
				<View style={{ ...styles.inputGroup, flex: 1 }}>
					<View style={styles.headerContainer}>
						<Text style={styles.headerText}>หลักฐานการโอนเงิน</Text>
						<TouchableOpacity style={styles.headerButton} onPress={onHandlePickImage}>
							<Text style={styles.headerButtonText}>
								{receiptImages ? 'อัพโหลดรูปภาพใหม่' : 'อัพโหลดรูปภาพ'}
							</Text>
						</TouchableOpacity>
					</View>
					{receiptImages ? (
						<Image
							resizeMode={'cover'}
							source={{ uri: receiptImages.uri }}
							style={styles.slipImage}
						/>
					) : (
						<View style={{ alignItems: 'center', paddingVertical: 30 }}>
							<Text style={{ color: '#A6A6A6', fontSize: 16 }}>ยังไม่มีหลักฐานการโอนเงิน</Text>
						</View>
					)}
				</View>
				<TouchableOpacity
					style={{ ...styles.button, backgroundColor: '#7DDFB5' }}
					onPress={() => onHandleSubmit(receiptImages, productsList, receiptDetail)}
				>
					<Text style={{ ...styles.buttonTitle, color: '#191919' }}>ออกใบเสร็จ</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	);
};

ReceiptCreateScreen.navigationOptions = () => {
	return {
		title: 'ออกใบเสร็จ',
		headerStyle: {
			backgroundColor: '#7DDFB5'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E8E5E0'
	},
	inputGroup: {
		flexDirection: 'column',
		justifyContent: 'space-around',
		backgroundColor: '#fff',
		marginTop: 5,
		padding: 5
	},
	headerContainer: {
		borderBottomWidth: 1,
		borderBottomColor: '#D98484',
		paddingVertical: 10,
		paddingHorizontal: 5,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	headerText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#191919'
	},
	headerButton: {
		backgroundColor: 'rgba(217, 132, 132, 0.7)',
		borderRadius: 50,
		paddingHorizontal: 10,
		paddingVertical: 5,
		marginVertical: -5
	},
	headerButtonText: {
		color: '#191919',
		fontSize: 16,
		fontWeight: 'bold'
	},
	button: {
		paddingVertical: 20,
		marginHorizontal: 40,
		marginVertical: 40,
		borderRadius: 50
	},
	buttonTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: 'rgb(40, 44, 52)',
		textAlign: 'center'
	},
	detailInput: {
		fontSize: 16,
		backgroundColor: 'rgba(217, 132, 132, 0.3)',
		borderRadius: 5,
		padding: 5
	},
	slipImage: {
		flex: 1,
		width: '100%',
		height: 400,
		borderRadius: 5,
		marginVertical: 5
	}
});

const mapStateToProps = state => ({
	productsList: state.receipt.productsList
});

export default connect(
	mapStateToProps,
	{ findProductByBarcode, removeItemFromReceipt, createReceipt, updateProduct }
)(ReceiptCreateScreen);
