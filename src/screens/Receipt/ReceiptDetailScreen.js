import React from 'react';
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { baseURL } from '../../api';

const ReceiptDetailScreen = ({ navigation }) => {
	const receipt = navigation.getParam('receipt');

	const calBackground = index => {
		if (index % 2) {
			return styles.oddContainer;
		} else {
			return styles.evenContainer;
		}
	};

	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={styles.receiptContainer}>
					<View
						style={{
							borderBottomColor: '#D98484',
							borderBottomWidth: 1,
							paddingBottom: 5,
							marginBottom: 5
						}}
					>
						<Text style={styles.receiptTitle}>ใบเสร็จเลขที่</Text>
					</View>
					<Text style={styles.receiptBody}>{receipt._id}</Text>
				</View>
				<View style={styles.receiptContainer}>
					<View
						style={{
							borderBottomColor: '#D98484',
							borderBottomWidth: 1,
							paddingBottom: 5,
							marginBottom: 5
						}}
					>
						<Text style={styles.receiptTitle}>รายการสินค้า</Text>
					</View>
					{receipt.list.map((item, index) => (
						<View key={index} style={calBackground(index)}>
							<View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
								<Text style={{ fontWeight: 'bold', fontSize: 16 }}>{item.quantity}x</Text>
							</View>
							<View style={{ flex: 1, paddingVertical: 5, paddingHorizontal: 10 }}>
								<Text style={{ fontSize: 16 }}>{item.name}</Text>
							</View>
							<View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
								<Text style={{ fontWeight: 'bold', fontSize: 16 }}>
									{item.price * item.quantity}
								</Text>
							</View>
						</View>
					))}
				</View>
				<View style={styles.receiptContainer}>
					<View
						style={{
							borderBottomColor: '#D98484',
							borderBottomWidth: 1,
							paddingBottom: 5,
							marginBottom: 5
						}}
					>
						<Text style={styles.receiptTitle}>รายละเอียดใบเสร็จ</Text>
					</View>
					{receipt.description.split('\n').map((text, index) => (
						<Text key={index} style={styles.receiptBody}>
							{text}
						</Text>
					))}
				</View>
				<View style={styles.receiptContainer}>
					<View
						style={{
							borderBottomColor: '#D98484',
							borderBottomWidth: 1,
							paddingBottom: 5,
							marginBottom: 5
						}}
					>
						<Text style={styles.receiptTitle}>หลักฐานการโอนเงิน</Text>
					</View>
					<Image
						resizeMode={'cover'}
						source={{
							uri: `${baseURL}/api/receipts/slip/${receipt.slip}`
						}}
						style={styles.slipImage}
					/>
				</View>
			</ScrollView>
		</View>
	);
};

ReceiptDetailScreen.navigationOptions = () => {
	return {
		title: 'รายละเอียดใบเสร็จ',
		headerStyle: {
			backgroundColor: '#D98484'
		}
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E8E5E0'
	},
	receiptContainer: {
		backgroundColor: '#fff',
		margin: 5,
		padding: 10
	},
	receiptTitle: {
		fontSize: 18,
		color: '#191919',
		fontWeight: 'bold'
	},
	receiptBody: {
		fontSize: 16
	},
	oddContainer: {
		flexDirection: 'row'
	},
	evenContainer: {
		flexDirection: 'row',
		backgroundColor: '#E9E9E9'
	},
	slipImage: {
		width: '100%',
		height: 400,
		borderRadius: 5
	},
	addButton: {
		position: 'absolute',
		bottom: 0,
		right: 0,
		zIndex: 10,
		marginRight: 10,
		marginBottom: 10,
		backgroundColor: '#D98484',
		height: 50,
		width: 50,
		alignContent: 'center',
		justifyContent: 'center',
		borderRadius: 50
	}
});

export default ReceiptDetailScreen;
