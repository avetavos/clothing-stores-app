import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { Text } from 'react-native-elements';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';

const BarcodeScannerScreen = ({ navigation }) => {
	const [hasCameraPermission, setCameraPermission] = useState(null);
	const [scanned, setScanned] = useState(false);
	let [quantity, setQuantity] = useState(1);

	useEffect(() => {
		getPermissionsAsync();
	}, []);

	const getPermissionsAsync = async () => {
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		setCameraPermission({ status: 'granted' });
	};

	const handleBarCodeScanned = async ({ type, data }) => {
		await setScanned(true);
		navigation.navigate(navigation.getParam('screen'), {
			barcodeValue: data,
			quantityValue: quantity
		});
	};

	if (hasCameraPermission === null) {
		return <Text>Requesting for camera permission</Text>;
	}

	if (hasCameraPermission === false) {
		return <Text>No access to camera</Text>;
	}

	const onUpQuantityValue = async () => {
		if (quantity >= 1) {
			setQuantity(quantity + 1);
		}
		return false;
	};

	const onDownQuantityValue = async () => {
		if (quantity > 1) {
			setQuantity(quantity - 1);
		}
		return false;
	};

	return (
		<View
			style={{
				flex: 1,
				justifyContent: 'flex-start'
			}}
		>
			<BarCodeScanner
				onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
				style={{ flex: 10 }}
			/>
			<View style={styles.quantityContainer}>
				<TouchableOpacity style={styles.quantityButton} onPress={() => onDownQuantityValue()}>
					<Text style={styles.quantityButtonText}>-</Text>
				</TouchableOpacity>
				<View style={{ flex: 1 }}>
					<TextInput editable={false} value={`${quantity}`} style={styles.quantityInput} />
				</View>
				<TouchableOpacity style={styles.quantityButton} onPress={() => onUpQuantityValue()}>
					<Text style={styles.quantityButtonText}>+</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	quantityContainer: {
		flex: 1,
		flexDirection: 'row',
		paddingHorizontal: 10
	},
	quantityButton: {
		height: 40,
		width: 40,
		backgroundColor: '#D98484',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center'
	},
	quantityInput: {
		textAlign: 'center',
		fontSize: 22,
		fontWeight: 'bold',
		backgroundColor: '#E9E9E9',
		borderRadius: 50,
		height: 40,
		marginHorizontal: 20
	},
	quantityButtonText: {
		fontSize: 22,
		fontWeight: 'bold',
		color: '#fff'
	}
});

BarcodeScannerScreen.navigationOptions = () => {
	return {
		title: `สแกนสินค้า`
	};
};

export default BarcodeScannerScreen;
