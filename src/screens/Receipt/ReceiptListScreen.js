import React, { useEffect } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Platform } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import { getAllReceipts } from '../../actions/receipt';
import ReceiptList from '../../components/ReceiptListComponent';

const ReceiptListScreen = ({ navigation, user, receipts, getAllReceipts }) => {
	useEffect(() => {
		getAllReceipts();
	}, []);

	let receiptsList =
		receipts && receipts.length > 0 ? (
			<FlatList
				data={receipts}
				renderItem={({ item }) => (
					<ReceiptList receipt={item} navigation={navigation} role={user.role} />
				)}
				keyExtractor={item => item._id}
			/>
		) : (
			<View style={styles.empty}>
				<Text h3>ไม่มีใบเสร็จ</Text>
			</View>
		);

	return (
		<SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
			{receiptsList}
			<TouchableOpacity
				style={styles.addButton}
				onPress={() => navigation.navigate('ReceiptCreate')}
			>
				<Icon name='plus' type='font-awesome' color='#000' />
			</TouchableOpacity>
		</SafeAreaView>
	);
};

ReceiptListScreen.navigationOptions = () => {
	return {
		header: null
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E8E5E0',
		paddingTop: Platform.OS === 'android' ? 25 : 0
	},
	empty: {
		marginTop: 50,
		alignItems: 'center',
		flex: 1
	},
	addButton: {
		position: 'absolute',
		bottom: 0,
		right: 0,
		zIndex: 10,
		marginRight: 10,
		marginBottom: 10,
		backgroundColor: '#D98484',
		height: 50,
		width: 50,
		alignContent: 'center',
		justifyContent: 'center',
		borderRadius: 50
	}
});

const mapStateToProps = state => ({
	receipts: state.receipt.receipts,
	user: state.auth.user
});

export default connect(
	mapStateToProps,
	{ getAllReceipts }
)(ReceiptListScreen);
