import React from 'react';
import { AsyncStorage } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';

import { Provider } from 'react-redux';
import store from './src/store';
import setAuthToken from './src/utils/setAuthToken';

import LoginScreen from './src/screens/LoginScreen';
import AuthenticationScreen from './src/screens/Auth/AuthenticationMainScreen';
import UpdateProfileScreen from './src/screens/Auth/UpdateProfileScreen';

import EmployeeListScreen from './src/screens/Employee/EmployeeListScreen';
import EmployeeCreateScreen from './src/screens/Employee/EmployeeCreateScreen';
import EmployeeEditScreen from './src/screens/Employee/EmployeeEditScreen';
import CoOwnerCreateScreen from './src/screens/Employee/CoOwnerCreateScreen';

import ProductListScreen from './src/screens/Product/ProductListScreen';
import ProductCreateScreen from './src/screens/Product/ProductCreateScreen';
import ProductDetailScreen from './src/screens/Product/ProductDetailScreen';
import BarcodeScannerScreen from './src/screens/Product/BarcodeScannerScreen';

import ReceiptCreateScreen from './src/screens/Receipt/ReceiptCreateScreen';
import ReceiptDetailScreen from './src/screens/Receipt/ReceiptDetailScreen';
import ReceiptListScreen from './src/screens/Receipt/ReceiptListScreen';
import BarcodeScannerScreen2 from './src/screens/Receipt/BarcodeScannerScreen';

import ResolveAuthScreen from './src/screens/ResolveAuthScreen';

import { setNavigator } from './src/navigationRef';

if (AsyncStorage.getItem('token')) {
	setAuthToken(AsyncStorage.getItem('token'));
}

const switchNavigator = createSwitchNavigator(
	{
		resolveAuth: ResolveAuthScreen,
		login: LoginScreen,
		resolveAuth: ResolveAuthScreen,
		login: LoginScreen,
		ownerFlow: createBottomTabNavigator(
			{
				Products: {
					screen: createStackNavigator({
						ProductList: ProductListScreen,
						ProductCreate: ProductCreateScreen,
						ProductDetail: ProductDetailScreen,
						BarcodeProduct: BarcodeScannerScreen
					}),
					navigationOptions: {
						title: 'สินค้า',
						tabBarIcon: ({ tintColor }) => <Icon name='ios-cube' type='ionicon' color={tintColor} />
					}
				},
				Receipts: {
					screen: createStackNavigator({
						ReceiptList: ReceiptListScreen,
						ReceiptCreate: ReceiptCreateScreen,
						ReceiptDetail: ReceiptDetailScreen,
						BarcodeReceipt: BarcodeScannerScreen2
					}),
					navigationOptions: ({ navigation }) => {
						let tabBarVisible = true;
						navigation.state.routes.forEach(routes => {
							if (routes.routeName == 'PrintReceipt') {
								tabBarVisible = false;
							}
						});
						return {
							tabBarVisible,
							title: 'ใบเสร็จ',
							tabBarIcon: ({ tintColor }) => (
								<Icon name='ios-paper' type='ionicon' color={tintColor} />
							)
						};
					}
				},
				Employees: {
					screen: createStackNavigator({
						MainEmployee: EmployeeListScreen,
						CreateEmployee: EmployeeCreateScreen,
						EditEmployee: EmployeeEditScreen,
						CreateCoOwner: CoOwnerCreateScreen
					}),
					navigationOptions: {
						title: 'พนักงาน',
						tabBarIcon: ({ tintColor }) => (
							<Icon name='ios-people' type='ionicon' color={tintColor} />
						)
					}
				},
				Authentication: {
					screen: createStackNavigator({
						MainAuth: AuthenticationScreen,
						UpdateProfile: UpdateProfileScreen
					}),
					navigationOptions: {
						title: 'ตั้งค่า',
						tabBarIcon: ({ tintColor }) => <Icon name='ios-cog' type='ionicon' color={tintColor} />
					}
				}
			},
			{
				tabBarOptions: {
					activeTintColor: '#F29C94',
					activeBackgroundColor: '#4D515D',
					inactiveTintColor: '#4D515D',
					inactiveBackgroundColor: '#D9D0C1'
				}
			}
		),
		employeeFlow: createBottomTabNavigator(
			{
				Receipts: {
					screen: createStackNavigator({
						ReceiptList: ReceiptListScreen,
						ReceiptCreate: ReceiptCreateScreen,
						ReceiptDetail: ReceiptDetailScreen,
						BarcodeReceipt: BarcodeScannerScreen2
					}),
					navigationOptions: {
						title: 'ใบเสร็จ',
						tabBarIcon: ({ tintColor }) => (
							<Icon name='ios-paper' type='ionicon' color={tintColor} />
						)
					}
				},
				Authentication: {
					screen: createStackNavigator({
						MainAuth: AuthenticationScreen
					}),
					navigationOptions: {
						title: 'ตั้งค่า',
						tabBarIcon: ({ tintColor }) => <Icon name='ios-cog' type='ionicon' color={tintColor} />
					}
				}
			},
			{
				tabBarOptions: {
					activeTintColor: '#F29C94',
					activeBackgroundColor: '#4D515D',
					inactiveTintColor: '#4D515D',
					inactiveBackgroundColor: '#D9D0C1'
				}
			}
		)
	},
	{
		initialRouteName: 'resolveAuth'
	}
);

const App = createAppContainer(switchNavigator);

export default () => {
	return (
		<Provider store={store}>
			<App
				ref={navigator => {
					setNavigator(navigator);
				}}
			/>
		</Provider>
	);
};
